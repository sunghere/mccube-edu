package com.ensof.mccube.msg;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import com.ensof.mccube.msg.TFA_CODE.TFALIST;

public class TransactionTfaCode {
	public static void main(String[] args) {
		try {
			TFA_CODE msg = new TFA_CODE();
			msg.setCOAL_CO_CD("9999916");
			msg.setCONT_YMD("20211130");
			msg.setORG_SLCT_PLNR_ID("TTT");
			TFALIST list = msg.addTFALIST(new TFALIST());
			list.setItem1("aaaaa");
//			String xmlData = msg.toXml();
//			System.out.println(xmlData);
//			String jsonData = msg.toJson();
//			System.out.println(jsonData);
//			
			String formData = msg.toForm();
			URL url = new URL("http://127.0.0.1:9091/api4home/ImgSignService");
			URLConnection con = url.openConnection();
			HttpURLConnection http = (HttpURLConnection)con;
			http.setRequestMethod("POST");
			
			byte[] out = formData.getBytes();
			int length = out.length;
			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=euc-kr");
			http.setDoOutput(true);
			try(OutputStream os = http.getOutputStream()) {
			    os.write(out);
			}
			
			InputStream inputStream = http.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String s = null;
			while((s = in.readLine())!=null) {
				System.out.println(s);
			}
			http.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
