<?xml version="1.0" encoding="euc-kr"?>
<packet arrayTitleSupport="false" firstElementRequired="false" firstElementRequiredIncludeRecord="false"
         javaPackage="" maskingChar="*" tagNumberPadding="false" unconditionalGroupToList="false"
         xmlSelfClosing="false" xmlStandalone="false" xmlVersion="1.0" zeroIsSpace="false">
    <string alias="" desc="고객정보수집동의여부" format="" name="COLLECT_INFO" nullable="true" padding=" "
             trim="false"/>
    <string alias="" desc="고객정보제공동의여부" format="" name="OFFER_INFO" nullable="true" padding=" "
             trim="false"/>
    <string alias="" desc="생년월일 기재여부" format="" name="BIRTH_DATE" nullable="true" padding=" "
             trim="false"/>
    <string alias="" desc="동의일자기입여부" format="" name="WRITE_DATE" nullable="true" padding=" "
             trim="false"/>
    <string alias="" format="" name="SMS_YN" nullable="true" padding=" " trim="false"/>
    <string alias="" format="" name="이메일" nullable="true" padding=" " trim="false"/>
    <string alias="" format="" name="TELE_YN" nullable="true" padding=" " trim="false"/>
    <string alias="" format="" name="POST_YN" nullable="true" padding=" " trim="false"/>
    <string alias="" format="" name="CHK_MEMO" nullable="true" padding=" " trim="false"/>
    <mapping>
        <element expression="" name="MsgGb"/>
        <element expression="" name="TrNo"/>
        <element expression="" name="TrCd"/>
        <element expression="" name="TxCd"/>
        <element expression="" name="ReqGb"/>
        <element expression="" name="ResCd"/>
        <element expression="" name="ResMsg"/>
        <element expression="" name="TraceNo"/>
        <element expression="" name="GroupKey"/>
        <element expression="" name="MatchingKey"/>
        <element expression="" name="Timeout"/>
        <element expression="" name="SAF"/>
        <element expression="" name="UserId"/>
        <element expression="" name="UserData"/>
        <element expression="" name="BizCd"/>
        <element expression="" name="Hold"/>
    </mapping>
</packet>