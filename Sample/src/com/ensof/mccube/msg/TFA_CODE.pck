<?xml version="1.0" encoding="euc-kr"?>
<packet arrayTitleSupport="false" firstElementRequired="false" firstElementRequiredIncludeRecord="false"
         javaPackage="" maskingChar="*" tagNumberPadding="false" unconditionalGroupToList="false"
         xmlSelfClosing="false" xmlStandalone="false" xmlVersion="1.0" zeroIsSpace="false">
    <tag alias="" name="root">
        <string alias="" desc="회사코드" format="" name="COAL_CO_CD" nullable="true" padding=" " trim="false"
                 xml="attribute"/>
        <string alias="" desc="제휴사 모집자 코드" format="" name="ORG_SLCT_PLNR_ID" nullable="true" padding=" "
                 trim="false" xml="attribute"/>
        <string alias="" desc="계약일" format="" name="CONT_YMD" nullable="true" padding=" " trim="false"
                 xml="attribute"/>
        <group alias="" format="" name="TFALIST" xmlTag="mandatory">
            <string alias="" format="" name="Item1" nullable="true" padding=" " trim="false" xml="attribute"/>
        </group>
    </tag>
    <mapping>
        <element expression="" name="MsgGb"/>
        <element expression="" name="TrNo"/>
        <element expression="" name="TrCd"/>
        <element expression="" name="TxCd"/>
        <element expression="" name="ReqGb"/>
        <element expression="" name="ResCd"/>
        <element expression="" name="ResMsg"/>
        <element expression="" name="TraceNo"/>
        <element expression="" name="GroupKey"/>
        <element expression="" name="MatchingKey"/>
        <element expression="" name="Timeout"/>
        <element expression="" name="SAF"/>
        <element expression="" name="UserId"/>
        <element expression="" name="UserData"/>
        <element expression="" name="BizCd"/>
        <element expression="" name="Hold"/>
    </mapping>
</packet>