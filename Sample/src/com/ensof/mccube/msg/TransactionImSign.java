package com.ensof.mccube.msg;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class TransactionImSign {
	public static void main(String[] args) {
		HttpURLConnection http = null;
		try {
			String encoding = System.getProperty("file.encoding");
			IMSIGN msg = new IMSIGN();
			msg.setBIRTH_DATE("20211011");
			msg.setCHK_MEMO("TEST");
			msg.setCOLLECT_INFO("AA");
			msg.set�̸���("Y");
			msg.setOFFER_INFO("BB");
			msg.setPOST_YN("N");
			msg.setSMS_YN("CC");
			msg.setWRITE_DATE("DD");
			
			String jsonData = msg.toJson();
			URL url = new URL("http://127.0.0.1:12001/api4home/ImgSignService");
			URLConnection con = url.openConnection();
			http = (HttpURLConnection)con;
			http.setRequestMethod("POST");
			
			byte[] out = jsonData.getBytes();
			int length = out.length;
			http.setFixedLengthStreamingMode(length);
			http.setRequestProperty("Content-Type", "application/json; charset=" + encoding);
			http.setDoOutput(true);
			OutputStream os = http.getOutputStream();
			os.write(out);
			
			
			InputStream inputStream = http.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			in.lines().forEach(System.out::println);
			
			// legacy
//			String s = null;
//			while((s = in.readLine())!=null) {
//				System.out.println(s);
//			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if(http !=null) {
				http.disconnect();
			}
		}
		
	}

	private static void closeSliently(Closeable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (Exception e) {
		}
	}
}
