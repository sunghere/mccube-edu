<?xml version="1.1" encoding="euc-kr"?>
<packet arrayTitleSupport="false" firstElementRequired="false" firstElementRequiredIncludeRecord="false"
         javaPackage="" maskingChar="*" tagNumberPadding="false" unconditionalGroupToList="false"
         xmlSelfClosing="false" xmlStandalone="false" xmlVersion="1.0" zeroIsSpace="false">
    <string alias="" format="" length="6" name="FIELD_1" nullable="true" padding=" " trim="false"/>
    <string alias="" format="" length="6" name="FIELD_2" nullable="true" padding=" " trim="false"/>
    <string alias="" format="" length="6" name="FIELD_3" nullable="true" padding=" " trim="false"/>
    <string alias="" format="" length="6" name="FIELD_4" nullable="true" padding=" " trim="false"/>
    <mapping>
        <element expression="" name="MsgGb"/>
        <element expression="" name="TrNo"/>
        <element expression="" name="TrCd"/>
        <element expression="" name="TxCd"/>
        <element expression="" name="ReqGb"/>
        <element expression="" name="ResCd"/>
        <element expression="" name="ResMsg"/>
        <element expression="" name="TraceNo"/>
        <element expression="" name="GroupKey"/>
        <element expression="" name="MatchingKey"/>
        <element expression="" name="Timeout"/>
        <element expression="" name="SAF"/>
        <element expression="" name="UserId"/>
        <element expression="" name="UserData"/>
        <element expression="" name="BizCd"/>
        <element expression="" name="Hold"/>
    </mapping>
</packet>